*** Settings ***
Force Tags
Library           SeleniumLibrary

*** Test Cases ***
connexion
    SeleniumLibrary.Open Browser    ${link-VM}    Chrome
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']

Flash news
    SeleniumLibrary.Click Link    xpath=//a[@href='/flashNews']
    Input Text    id:appbundle_flashnews_title    test1
    Input Text    id:appbundle_flashnews_description    Automatisation des tests du projet"Visual Management" \
    Click Element    xpath=//span[@class='glyphicon glyphicon-calendar']
    Input Text    name:datetimepicker7    2019-05-15 12:02:32

searching flash news
    Element Should Contain    xpath=//ul[@id='webTicker']    test1: Automatisation des tests du projet"Visual Management"
    Close Browser
