*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${link1}          http://visual.management.loc/
${link2}          http://visual.management.loc/criticals
${link3}          http://visual.management.loc/config
${link4}          http://visual.management.loc/flashNews
${link5}          http://visual.management.loc/charts
${link6}          http://visual.management.loc/graphes

*** Test Cases ***
Dashboard
    SeleniumLibrary.Open Browser    ${link1}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Element Text Should Be    xpath=//h2[@class='page-header']    ENTSOG Reporting
    Click Link    xpath=//a[@href='/logout']
    Close Browser

Criticals
    SeleniumLibrary.Open Browser    ${link2}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Element Should Contain    xpath=//div[@class='panel-heading']//b    Criticals of the month
    Click Link    xpath=//a[@href='/logout']
    Close Browser

SLA config
    SeleniumLibrary.Open Browser    ${link3}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Element Should Contain    xpath=//div[contains(text(),'SLA standards configuration')]    SLA standards configuration
    Click Link    xpath=//a[@href='/logout']
    Close Browser

flash news
    SeleniumLibrary.Open Browser    ${link4}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Element Should Contain    xpath=//div[contains(text(),'flash news')]    flash news
    Click Link    xpath=//a[@href='/logout']
    Close Browser

Tables
    SeleniumLibrary.Open Browser    ${link5}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Click Link    xpath=//a[@href='/logout']
    Close Browser

charts
    SeleniumLibrary.Open Browser    ${link6}    Chrome
    Element Text Should Be    xpath=//h3[@class='panel-title']    Please Sign In
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Element Text Should Be    xpath=//h1[@class='page-header']    Charts
    Click Link    xpath=//a[@href='/logout']
    Close Browser
