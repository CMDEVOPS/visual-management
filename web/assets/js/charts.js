<<<<<<< HEAD
function renderCharts(data, months) {
    var totalDataPoints = [];
    var closedDataPoints = [];

    for (var i = 0; i < data.length; i++) {
        totalDataPoints[i] = {
            label: months[i],
            y: data[i].TotalTickets !== undefined ? parseInt(data[i].TotalTickets) : 0,
        };

        closedDataPoints[i] = {
            label: months[i],
            y:  data[i].ClosedTickets !== undefined ? parseInt(data[i].ClosedTickets) : 0,
        };
    }

    var chart = new CanvasJS.Chart("chartContainer",
        {
            title: {
                text: "Tickets By Month",
                fontColor: "#000000"
            },
            data: [
                {
                    type: "line",
                    showInLegend: true,
                    legendText: "Total",
                    dataPoints: totalDataPoints,
                },
                {
                    type: "line",
                    showInLegend: true,
                    legendText: "Closed",
                    dataPoints: closedDataPoints,
                }
            ]
        });
    var chart2 = new CanvasJS.Chart("PieContainer", {
        animationEnabled: true,
        title: {
            text: "Another chart to add"
        },
        data: [{
            type: "doughnut",
            innerRadius: "40%",
            showInLegend: true,
            legendText: "{label}",
            indexLabel: "{label}: #percent%",
            dataPoints: [
                {label: "Label1", y: 6492917},
                {label: "Label2", y: 7380554},
                {label: "Label3", y: 1610846},
                {label: "Label4", y: 950875},
                {label: "Label5", y: 900000},
                {label: "Label6", y: 800000},
                {label: "Label7", y: 700000}
            ]
        }]
    });
    var chart3 = new CanvasJS.Chart("ColumnContainer", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Simple Column Chart with Index Labels"
        },
        data: [{
            type: "column", //change type to bar, line, area, pie, etc
            //indexLabel: "{y}", //Shows y value on all Data Points
            indexLabelFontColor: "#5A5757",
            indexLabelPlacement: "outside",
            dataPoints: [
                {x: 10, y: 71},
                {x: 20, y: 55},
                {x: 30, y: 50},
                {x: 40, y: 65},
                {x: 50, y: 92, indexLabel: "Highest"},
                {x: 60, y: 68},
                {x: 70, y: 38},
                {x: 80, y: 71},
                {x: 90, y: 54},
                {x: 100, y: 60},
                {x: 110, y: 36},
                {x: 120, y: 49},
                {x: 130, y: 21, indexLabel: "Lowest"}
            ]
        }]
    });
    var chart4 = new CanvasJS.Chart("BarContainer", {
        animationEnabled: true,

        title: {
            text: "Bar Chat to add"
        },
        axisX: {
            interval: 1
        },
        axisY2: {
            interlacedColor: "rgba(1,77,101,.2)",
            gridColor: "rgba(1,77,101,.1)",
            title: "Number of Companies"
        },
        data: [{
            type: "bar",
            name: "companies",
            axisYType: "secondary",
            color: "#014D65",
            dataPoints: [
                {y: 3, label: "Sweden"},
                {y: 7, label: "Taiwan"},
                {y: 5, label: "Russia"},
                {y: 9, label: "Spain"},
                {y: 7, label: "Brazil"},
                {y: 7, label: "India"},
                {y: 9, label: "Italy"},
                {y: 8, label: "Australia"},
                {y: 11, label: "Canada"},
                {y: 15, label: "South Korea"},
                {y: 12, label: "Netherlands"},
                {y: 15, label: "Switzerland"},
                {y: 25, label: "Britain"},
                {y: 28, label: "Germany"},
                {y: 29, label: "France"},
                {y: 52, label: "Japan"},
                {y: 103, label: "China"},
                {y: 134, label: "US"}
            ]
        }]
    });

    chart.render();
    chart2.render();
    chart3.render();
    chart4.render();
}

function renderMorrisAreas(data, months) {
    var dataPoints = [];

    for (var i = 0; i < data.length; i++) {
        dataPoints[i] = {
            y: months[i],
            a: data[i].TotalTickets !== undefined ? parseInt(data[i].TotalTickets) : 0,
            b: data[i].ClosedTickets !== undefined ? parseInt(data[i].ClosedTickets) : 0,
        };
    }
    Morris.Area({
        element: 'area-example',
        data: dataPoints,
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B']
    });
    // When you open modal several times Morris charts over loading. So this is for destory to over loades Morris charts.
    // If you have better way please share it.
    if ($('#area-example').find('svg').length > 1) {
        // Morris Charts creates svg by append, you need to remove first SVG
        $('#area-example svg:first').remove();
        // Also Morris Charts created for hover div by prepend, you need to remove last DIV
        $(".morris-hover:last").remove();
    }
    // Smooth Loading
    $('.js-loading').addClass('hidden');
=======
function renderCharts(data, months) {
    var totalDataPoints = [];
    var closedDataPoints = [];

    for (var i = 0; i < data.length; i++) {
        totalDataPoints[i] = {
            label: months[i],
            y: data[i].TotalTickets !== undefined ? parseInt(data[i].TotalTickets) : 0,
        };

        closedDataPoints[i] = {
            label: months[i],
            y:  data[i].ClosedTickets !== undefined ? parseInt(data[i].ClosedTickets) : 0,
        };
    }

    var chart = new CanvasJS.Chart("chartContainer",
        {
            title: {
                text: "Tickets By Month",
                fontColor: "#000000"
            },
            data: [
                {
                    type: "line",
                    showInLegend: true,
                    legendText: "Total",
                    dataPoints: totalDataPoints,
                },
                {
                    type: "line",
                    showInLegend: true,
                    legendText: "Closed",
                    dataPoints: closedDataPoints,
                }
            ]
        });
    var chart2 = new CanvasJS.Chart("PieContainer", {
        animationEnabled: true,
        title: {
            text: "Another chart to add"
        },
        data: [{
            type: "doughnut",
            innerRadius: "40%",
            showInLegend: true,
            legendText: "{label}",
            indexLabel: "{label}: #percent%",
            dataPoints: [
                {label: "Label1", y: 6492917},
                {label: "Label2", y: 7380554},
                {label: "Label3", y: 1610846},
                {label: "Label4", y: 950875},
                {label: "Label5", y: 900000},
                {label: "Label6", y: 800000},
                {label: "Label7", y: 700000}
            ]
        }]
    });
    var chart3 = new CanvasJS.Chart("ColumnContainer", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Simple Column Chart with Index Labels"
        },
        data: [{
            type: "column", //change type to bar, line, area, pie, etc
            //indexLabel: "{y}", //Shows y value on all Data Points
            indexLabelFontColor: "#5A5757",
            indexLabelPlacement: "outside",
            dataPoints: [
                {x: 10, y: 71},
                {x: 20, y: 55},
                {x: 30, y: 50},
                {x: 40, y: 65},
                {x: 50, y: 92, indexLabel: "Highest"},
                {x: 60, y: 68},
                {x: 70, y: 38},
                {x: 80, y: 71},
                {x: 90, y: 54},
                {x: 100, y: 60},
                {x: 110, y: 36},
                {x: 120, y: 49},
                {x: 130, y: 21, indexLabel: "Lowest"}
            ]
        }]
    });
    var chart4 = new CanvasJS.Chart("BarContainer", {
        animationEnabled: true,

        title: {
            text: "Bar Chat to add"
        },
        axisX: {
            interval: 1
        },
        axisY2: {
            interlacedColor: "rgba(1,77,101,.2)",
            gridColor: "rgba(1,77,101,.1)",
            title: "Number of Companies"
        },
        data: [{
            type: "bar",
            name: "companies",
            axisYType: "secondary",
            color: "#014D65",
            dataPoints: [
                {y: 3, label: "Sweden"},
                {y: 7, label: "Taiwan"},
                {y: 5, label: "Russia"},
                {y: 9, label: "Spain"},
                {y: 7, label: "Brazil"},
                {y: 7, label: "India"},
                {y: 9, label: "Italy"},
                {y: 8, label: "Australia"},
                {y: 11, label: "Canada"},
                {y: 15, label: "South Korea"},
                {y: 12, label: "Netherlands"},
                {y: 15, label: "Switzerland"},
                {y: 25, label: "Britain"},
                {y: 28, label: "Germany"},
                {y: 29, label: "France"},
                {y: 52, label: "Japan"},
                {y: 103, label: "China"},
                {y: 134, label: "US"}
            ]
        }]
    });

    chart.render();
    chart2.render();
    chart3.render();
    chart4.render();
}

function renderMorrisAreas(data, months) {
    var dataPoints = [];

    for (var i = 0; i < data.length; i++) {
        dataPoints[i] = {
            y: months[i],
            a: data[i].TotalTickets !== undefined ? parseInt(data[i].TotalTickets) : 0,
            b: data[i].ClosedTickets !== undefined ? parseInt(data[i].ClosedTickets) : 0,
        };
    }
    Morris.Area({
        element: 'area-example',
        data: dataPoints,
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B']
    });
    // When you open modal several times Morris charts over loading. So this is for destory to over loades Morris charts.
    // If you have better way please share it.
    if ($('#area-example').find('svg').length > 1) {
        // Morris Charts creates svg by append, you need to remove first SVG
        $('#area-example svg:first').remove();
        // Also Morris Charts created for hover div by prepend, you need to remove last DIV
        $(".morris-hover:last").remove();
    }
    // Smooth Loading
    $('.js-loading').addClass('hidden');
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
}