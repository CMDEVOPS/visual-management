*** Settings ***
Library           SeleniumLibrary

*** Test Cases ***
connexion
    SeleniumLibrary.Open Browser    ${link-VM}    Chrome
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Element    xpath=//input[@value='Remember Me']
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Click Link    xpath=//a[@href='/logout']
    Input Text    name:_username    entsog
    Element Should Contain    _password    entsog93
