SILENT:
.PHONY: build test security deploy

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

###########
# Install #
###########

## Install application
install:
	# Composer
	composer install --verbose
	# Db
	bin/console doctrine:database:create --if-not-exists
	bin/console doctrine:schema:update --force
	# Db - Test
	bin/console doctrine:database:create --if-not-exists --env=test
	bin/console doctrine:schema:update --force --env=test
	# Db - Fixtures
	#bin/console doctrine:fixtures:load --no-interaction
	# Db - Fixtures - Test
	#bin/console doctrine:fixtures:load --no-interaction --env=test

install@prod: export SYMFONY_ENV = prod
install@prod:
	# Composer
	composer install --verbose --no-progress --no-interaction --prefer-dist --optimize-autoloader --no-dev

## Database installation
database:
	bin/console doctrine:schema:update --force

database@prod: export SYMFONY_ENV = prod
database@prod:
	# Create database if not exist
	bin/console doctrine:database:create --if-not-exists
	# Update database
	bin/console doctrine:schema:update --force
	# Symfony cache
	bin/console cache:clear --no-debug

## Javascript Librairies Installation
install-libs:
	# Install node modules
	yarn install

## Install Assets
install-assets:
	# Install bundle assets
	bin/console assets:install web --symlink
	# Dump assets
	bin/console assetic:dump

install-assets@prod: export SYMFONY_ENV = prod
install-assets@prod:
	# Install bundle assets
	bin/console assets:install web --symlink
	# Dump assets
	bin/console assetic:dump
	# Symfony cache
	bin/console cache:warmup --no-debug

## Update application
update:
	composer update
update@prod: export SYMFONY_ENV = prod
update@prod:
	# Composer
	composer update --verbose --no-progress --no-interaction --prefer-dist --optimize-autoloader --no-dev
	# Symfony cache
	bin/console cache:warmup --no-debug

###############
# Permissions #
###############

permissions:
	$(eval HTTPDUSER = $(shell ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1))
	$(eval WHOAMI = $(shell whoami))
	$(eval DIR = $(shell pwd))
	# Give Read Write Execute permissions for var directory
	setfacl -dR -m u:$(HTTPDUSER):rwX -m u:$(WHOAMI):rwX $(DIR)/var
	setfacl -R -m u:$(HTTPDUSER):rwX -m u:$(WHOAMI):rwX $(DIR)/var

############
# Security #
############

## Run security checks
security:
	bin/console security:check

## Check Requirements
check-req:
	bin/symfony_requirements

########
# Lint #
########

## Run lint tools
lint:
	php-cs-fixer fix --config-file=.php_cs --dry-run --diff

lint@test: export SYMFONY_ENV = test
lint@test: lint

## Run Code Sniffer
code-sniffer:
	phpcs --standard=Symfony ./src -v

## Run PHP Mess Detector
mess-detector:
	phpmd ./src text codesize,unusedcode --reportfile phpmd.txt

########
# Test #
########

## Run tests
test: export SYMFONY_ENV = test
test:
	# PHPUnit
	phpunit

test@test: export SYMFONY_ENV = test
test@test:
	# PHPUnit
	rm -Rf build/phpunit && mkdir -p build/phpunit
	stty cols 80 && phpunit --log-junit build/phpunit/junit.xml --coverage-clover build/phpunit/clover.xml --coverage-html build/phpunit/coverage

##############
# Deployment #
##############

## Deploy project for production
deploy: check-req install@prod database@prod install-libs install-assets@prod permissions
