<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Utils;

/**
 * Class Calculator.
 */
class Calculator
{
    /**
     * @param array $row
     * @param int   $resolution
     *
     * @return array
     */
    public function calculateSlaTime(array $row, $resolution)
    {
        $spendingTime = $row['ResponseTimeStatus3'] - $row['TimeEntsogclosed'];

<<<<<<< HEAD
        $timeh = (int) ($spendingTime / 3600);
        $timem = (int) ($spendingTime % 3600 / 60);
        $times = (int) ($spendingTime % (3600 / 60));

        $row['slaAbove'] = max($timeh - $resolution, 0);
=======
        $timeh = (int)($spendingTime / 3600);
        $timem = (int)($spendingTime % 3600 / 60);
        $times = (int)($spendingTime % (3600 / 60));

        $row['slaAbove'] = max($timeh - ($resolution), 0);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $row['slaToShow'] = $timeh - $resolution.':'.$timem.':'.$times;
        $row['timeResolution'] = $resolution;
        $row['remainingTime'] = $this->calculateRemainingTime($spendingTime, $resolution);
        $row['spentTime'] = $timeh.':'.$timem.':'.$times;

        return $row;
    }

    /**
     * @param int $spendingTime
     * @param int $resolution
     *
     * @return string
     */
    public function calculateRemainingTime($spendingTime, $resolution)
    {
<<<<<<< HEAD
        $remainingTime = ($resolution * 3600) - $spendingTime;
=======
        $remainingTime = ($resolution * 3600) - ($spendingTime);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        $hours = (int) ($remainingTime / 3600);
        $minutes = (int) ($remainingTime % 3600 / 60);
        $seconds = (int) ($remainingTime % (3600 / 60));

        return $hours.':'.$minutes.':'.$seconds;
    }
}
