<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class SecurityController.
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     *
     * @param SessionInterface          $session
     * @param CsrfTokenManagerInterface $tokenManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(SessionInterface $session, CsrfTokenManagerInterface $tokenManager)
    {
        $error = null;
        $csrfToken = $tokenManager ? $tokenManager->getToken('authenticate')->getValue() : null;
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey = Security::LAST_USERNAME);

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
        ]);
    }
}
