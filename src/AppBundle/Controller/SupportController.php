<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SupportController.
 */
class SupportController extends Controller
{
}
