<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Api;

use AppBundle\Manager\ConnectionManager;
use AppBundle\Manager\IssueManager;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
<<<<<<< HEAD
use Symfony\Component\HttpFoundation\Session\SessionInterface;
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

/**
 * Class IssueController.
 */
class IssueController extends FOSRestController
{
<<<<<<< HEAD
    private $currentMonth;

    private $currentYear;

    private $connectionManager;

    /**
     * @param SessionInterface  $session
     * @param ConnectionManager $connectionManager
     */
    public function __construct(SessionInterface $session, ConnectionManager $connectionManager)
    {
        $this->currentMonth = $session->has('current_month') ? $session->get('current_month') :  $session->set('current_month',date('m'));
        $this->currentYear = $session->has('current_year') ? $session->get('current_year') :  $session->set('current_month',date('Y'));
=======
    /**
     * @var ConnectionManager
     */
    private $connectionManager;

    /**
     * @param ConnectionManager $connectionManager
     */
    public function __construct(ConnectionManager $connectionManager)
    {
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $this->connectionManager = $connectionManager;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postViewAction(Request $request)
    {
<<<<<<< HEAD
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
        $severity = $request->request->get('severity');
        $toShow = [];

        $result = $this->connectionManager->issueStatusOpened($currentMonth, $currentYear);
=======
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
        $chosenMonth = 3; //what's the point from this ?
        $severity = $request->request->get('severity');
        $toShow = [];

        $result = $this->connectionManager->issueStatus($currentMonth, $currentYear);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        foreach ($result as $row) {
            if ($row['Severity'] === $severity) {
                $toShow[] = $row;
            }
        }

<<<<<<< HEAD
        return $this->render('parts/sides/table_body.html.twig', [
=======
        return $this->render('parts/sides/tableBody.html.twig', [
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            'result' => $result,
            'toShow' => $toShow,
            'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
<<<<<<< HEAD
=======
            'chosenMonth' => $chosenMonth,
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postFilterMonthAction(Request $request)
    {
        $choosenDate = $request->request->get('m');
        $date = explode('-', $choosenDate);
<<<<<<< HEAD
        $year = (int) $date[0];
        $month = (int) $date[1];
        $result = $this->connectionManager->assigneeTicketsPriority($month, $year);
=======
        $year = (int) ($date[0]);
        $month = (int) ($date[1]);

        $result = $this->connectionManager->executeTicketsPriorityProc($month, $year);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        return $this->render('parts/priority.html.twig', [
            'table' => $result,
            'choosen_date' => $choosenDate,
        ]);
    }
<<<<<<< HEAD


    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postTrackerViewAction(Request $request)
    {
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
        $tracker = $request->request->get('tracker');
        $toShow = [];

        $result = $this->connectionManager->issueStatusOpened($currentMonth, $currentYear);
        foreach ($result as $row) {
            if (IssueManager::resolveTracker($row['Tracker']) === $tracker) {
                $toShow[] = $row;
            }
        }

        return $this->render('parts/sides/table_body.html.twig', [
            'result' => $result,
            'toShow' => $toShow,
            'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
        ]);
    }
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
}
