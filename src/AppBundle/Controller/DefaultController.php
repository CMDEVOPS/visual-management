<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Flashnews;
use AppBundle\Entity\Project;
use AppBundle\Entity\SlaStandards;
use AppBundle\Form\FlashNewsType;
use AppBundle\Form\SlaStandardsType;
use AppBundle\Manager\ConnectionManager;
use AppBundle\Manager\IssueManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
<<<<<<< HEAD
use Symfony\Component\HttpFoundation\Session\SessionInterface;
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

/**
 * Class DefaultController.
 */
class DefaultController extends Controller
{
<<<<<<< HEAD
    private $currentMonth;

    private $currentYear;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->currentMonth = $session->has('current_month') ? $session->get('current_month') : date('m');
        $this->currentYear = $session->has('current_year') ? $session->get('current_year') : date('Y');
    }

=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
    /**
     * @Route("/", name="homepage")
     *
     * @param ConnectionManager $connectionManager
     * @param IssueManager      $issueManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ConnectionManager $connectionManager, IssueManager $issueManager)
    {
<<<<<<< HEAD
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
        $chosenMonth = 3;

        $result = $connectionManager->issueStatusOpened($currentMonth, $currentYear);
        $tracker = $issueManager::countIssuesByTracker($result);
        $flashNews = $connectionManager->findActiveFlashNews();

        return $this->render('default/home.html.twig', [
            'result' => $result,
            'tracker' => $tracker,
=======
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
        $chosenMonth = 3;

        $result = $connectionManager->issueStatus($currentMonth, $currentYear);
        $issues = $issueManager->countIssuesBySeverity($result);
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();

        return $this->render('default/home.html.twig', [
            'result' => $result,
            'minors' => $issues['minors'],
            'majors' => $issues['majors'],
            'criticals' => $issues['criticals'],
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
            'chosenMonth' => $chosenMonth,
            'flash' => $flashNews,
        ]);
    }

    /**
<<<<<<< HEAD
     * @Route("/criticals", name="accueil")
=======
     * @Route("/accueil", name="accueil")
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     *
     * @param ConnectionManager $connectionManager
     * @param IssueManager      $issueManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
<<<<<<< HEAD
    public function criticalAction(ConnectionManager $connectionManager, IssueManager $issueManager)
    {
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
=======
    public function accueilAction(ConnectionManager $connectionManager, IssueManager $issueManager)
    {
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $chosenMonth = 3;

        $result = $connectionManager->issueStatus($currentMonth, $currentYear);
        $issues = $issueManager->sortIssuesBySeverity($result);
        $criticity = $issueManager->sortIssuesByCriticity($issues['criticals']);
<<<<<<< HEAD
        $flashNews = $connectionManager->findActiveFlashNews();
=======

        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        return $this->render('parts/accueil.html.twig', [
            'result' => $result,
            'minors' => count($issues['minors']),
            'majors' => count($issues['majors']),
            'criticals' => count($issues['criticals']),
            'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
            'chosenMonth' => $chosenMonth,
            'flash' => $flashNews,
            'red' => $criticity['red'],
            'yellow' => $criticity['yellow'],
            'blue' => $criticity['blue'],
        ]);
    }

    /**
     * @Route("/config", name="config")
     *
     * @param Request           $request
     * @param ConnectionManager $connectionManager
     * @param IssueManager      $issueManager
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configurationAction(Request $request, ConnectionManager $connectionManager, IssueManager $issueManager)
    {
<<<<<<< HEAD
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
=======
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
        // $chosenMonth = 3;
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $sla = new SlaStandards();

        $form = $this->createForm(SlaStandardsType::class, $sla);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $connectionManager->getCustomerManager()->persist($sla);
            $connectionManager->getCustomerManager()->flush();

            return $this->redirectToRoute('config');
        }

        $result = $connectionManager->issueStatus($currentMonth, $currentYear);
        $issues = $issueManager->countIssuesBySeverity($result);
<<<<<<< HEAD
        $flashNews = $connectionManager->findActiveFlashNews();
=======
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $configs = $connectionManager->getCustomerManager()->getRepository(SlaStandards::class)->findAll();

        return $this->render('default/configuration.html.twig', [
            'form_add' => $form->createView(),
            'result' => $result,
            'minors' => $issues['minors'],
            'majors' => $issues['majors'],
            'criticals' => $issues['criticals'],
            'configs' => $configs,
            'flash' => $flashNews,
        ]);
    }

    /**
     * @Route("/delete", name="delete")
     *
     * @param Request           $request
     * @param ConnectionManager $connectionManager
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ConnectionManager $connectionManager)
    {
        $config = $connectionManager->getCustomerManager()->getRepository(SlaStandards::class)->find(($request->get('id')));
        $connectionManager->getCustomerManager()->remove($config);
        $connectionManager->getCustomerManager()->flush();

        return $this->redirectToRoute('config');
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request           $request
     * @param ConnectionManager $connectionManager
     * @param IssueManager      $issueManager
     * @param int               $id
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, ConnectionManager $connectionManager, IssueManager $issueManager, $id)
    {
<<<<<<< HEAD
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;
        // $chosenMonth = 3;

        $conf = $connectionManager->getCustomerManager()->getRepository(SlaStandards::class)->find($id);
        $result = $connectionManager->issueStatus($currentMonth, $currentYear);
=======
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
        // $chosenMonth = 3;

        $conf = $connectionManager->getCustomerManager()->getRepository(SlaStandards::class)->find($id);
        $result = $connectionManager->executeVisualProc($currentMonth, $currentYear);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $issues = $issueManager->countIssuesBySeverity($result);

        $form = $this->createForm(SlaStandardsType::class, $conf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('Valider')->isClicked()) {
                $connectionManager->getCustomerManager()->persist($conf);
                $connectionManager->getCustomerManager()->flush();

                return $this->redirectToRoute('config');
            }
        }

        return $this->render('default/SLAStandards/edit_sla_config.html.twig', [
            'form' => $form->createView(),
            'result' => $result,
            'minors' => $issues['minors'],
            'majors' => $issues['majors'],
            'criticals' => $issues['criticals'],
            'conf' => $conf,
        ]);
    }

    /**
<<<<<<< HEAD
     * @Route("/flashNews", name="flash_news")
=======
     * @Route("/flashNews", name="flashNews")
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     *
     * @param Request           $request
     * @param ConnectionManager $connectionManager
     * @param IssueManager      $issueManager
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function flashNewsAction(Request $request, ConnectionManager $connectionManager, IssueManager $issueManager)
    {
<<<<<<< HEAD
        $currentYear = (int) $this->currentYear;

        $result = $connectionManager->slaDetailled($currentYear);
        $issues = $issueManager->countIssuesBySeverity($result);
        $projects = $connectionManager->getCustomerManager()->getRepository(Project::class)->findAll();
        $flashNewsList = $connectionManager->findActiveFlashNews();
=======
        //$currentMonth = date('m');
        $currentYear = date('Y');

        $result = $connectionManager->executeSLAProc($currentYear);
        $issues = $issueManager->countIssuesBySeverity($result);
        $projects = $connectionManager->getCustomerManager()->getRepository(Project::class)->findAll();
        $flashNewsList = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        $flashNews = new Flashnews();
        $form = $this->createForm(FlashNewsType::class, $flashNews);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateStart = $request->get('datetimepicker6');
            $dateEnd = $request->get('datetimepicker7');
            $flashNews->setDated(new \DateTime($dateStart));
            $flashNews->setDatef(new \DateTime($dateEnd));

            $connectionManager->getCustomerManager()->persist($flashNews);
            $connectionManager->getCustomerManager()->flush();

            return $this->redirectToRoute('homepage');
        }

<<<<<<< HEAD
        return $this->render('default/flash_news.html.twig', [
=======
        return $this->render('default/flashNews.html.twig', [
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            'form' => $form->createView(),
            'result' => $result,
            'minors' => $issues['minors'],
            'majors' => $issues['majors'],
            'criticals' => $issues['criticals'],
            'projects' => $projects,
            'flash' => $flashNewsList,
        ]);
    }

    /**
     * @Route("/charts", name="charts")
     *
     * @param ConnectionManager $connectionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function chartAction(ConnectionManager $connectionManager)
    {
<<<<<<< HEAD
        $currentMonth = (int) $this->currentMonth;
        $currentYear = (int) $this->currentYear;

        $flashNews = $connectionManager->findActiveFlashNews();
        $issues1 = $connectionManager->rendementByMonth();
        $issues = IssueManager::getMonthNameByNumber($issues1);

        $issuesTicketsStatus = $connectionManager->assigneeTicketsStatus();
        $issuesTicketsPriority = $connectionManager->assigneeTicketsPriority($currentMonth, $currentYear);
=======
        $currentMonth = (int) IssueManager::SLA_STARTING_MONTH;
        $currentYear = (int) IssueManager::SLA_STARTING_YEAR;
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();

        $issues1 = $connectionManager->executeRendementByMonthProc();
        $issues = IssueManager::getMonthNameByNumber($issues1);

        $issuesTicketsStatus = $connectionManager->executeTicketsStatusProc();
        $issuesTicketsPriority = $connectionManager->executeTicketsPriorityProc($currentMonth, $currentYear);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        $choosenDate = $currentYear.'-'.$currentMonth;

        return $this->render('parts/charts.html.twig', [
            'issues' => $issues,
            'issues_status' => $issuesTicketsStatus,
            'issues_priority' => $issuesTicketsPriority,
            'flash' => $flashNews,
            'choosen_date' => $choosenDate,
        ]);
    }

    /**
     * @Route("/graphes", name="graphes")
     *
     * @param ConnectionManager $connectionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function grapheAction(ConnectionManager $connectionManager)
    {
<<<<<<< HEAD
        $issues = $connectionManager->rendementByMonth();
        $flashNews = $connectionManager->findActiveFlashNews();
=======
        $issues = $connectionManager->executeRendementByMonthProc();
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

        return $this->render('parts/sides/graphes.html.twig', [
            'issues' => $issues,
            'flash' => $flashNews,
        ]);
    }

    /**
<<<<<<< HEAD
     * @Route("/taskActivity", name="task_activity")
=======
     * @Route("/taskActivity", name="taskActivity")
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     *
     * @param ConnectionManager $connectionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function taskActivityAction(ConnectionManager $connectionManager)
    {
<<<<<<< HEAD
        $result = $connectionManager->ticketsByRessources();
        $flashNews = $connectionManager->findActiveFlashNews();

        return $this->render('default/task_activity.html.twig', [
=======
        $result = $connectionManager->executeTicketsByRessourcesProc();
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();

        return $this->render('default/taskActivity.html.twig', [
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            'flash' => $flashNews,
            'results' => $result,
        ]);
    }

    /**
<<<<<<< HEAD
     * @Route("/statisticActivity", name="statistics_activity")
=======
     * @Route("/statisticActivity", name="statisticsActivity")
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     *
     * @param ConnectionManager $connectionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function statisticsActivityAction(ConnectionManager $connectionManager)
    {
<<<<<<< HEAD
        $result = $connectionManager->treatedTicketsByRessources();
        $flashNews = $connectionManager->findActiveFlashNews();

        return $this->render('default/statistics_activity.html.twig', [
=======
        $result = $connectionManager->executeTreatedTicketsByRessourcesProc();
        $flashNews = $connectionManager->getCustomerManager()->getRepository(Flashnews::class)->findAll();

        return $this->render('default/statisticsActivity.html.twig', [
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            'flash' => $flashNews,
            'result' => $result,
        ]);
    }
<<<<<<< HEAD

    /**
     * @Route("/calendar", name="calendar")
     *
     * @param Request          $request
     * @param SessionInterface $session
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function calendarAction(Request $request, SessionInterface $session)
    {
        if ($request->request->has('calendarpicker')) {
            $date = $request->request->get('calendarpicker');
            $calendar = explode('-', $date);

            isset($calendar[0]) ? $session->set('current_month', $calendar[0]) : $session->set('current_month', date('m'));
            isset($calendar[1]) ? $session->set('current_year', $calendar[1]) : $session->set('current_year',  date('Y'));
        }

        return $this->redirectToRoute('homepage');
    }
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
}
