<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConnectToRedmineCommand.
 */
class ConnectToRedmineCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('redmine:connect')
            ->setDescription('Connect to redmine');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $this->getContainer()->get('app.default_controller');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function executeFromRedmine(InputInterface $input, OutputInterface $output)
    {
        // $logger = $this->getContainer()->get('logger');
        $output->writeln('######### 1 #########');
        $emMysql = $this->getContainer()->get('doctrine')->getManager()->getConnection();
        $output->writeln('######### 2 #########');
        $sth = $emMysql->prepare('CALL SLA_Detailled(05,2017)');
        $output->writeln('######### 3 #########');
        $sth->execute();
        $output->writeln('######### 4 #########');

        $result = $sth->fetchAll();
        foreach ($result as $row) {
            $output->writeln($row['Tracker']);
            $output->writeln($row['project']);
            $output->writeln($row['Severity']);
            $output->writeln($row['id']);
            $output->writeln($row['subject']);
            $output->writeln($row['CreationDT']);
            $output->writeln($row['IdentifiedtoReproducedDT']);
            $output->writeln($row['ResponseTimeStatus1']);
            $output->writeln($row['Testedtodeliver']);
            $output->writeln($row['ResponseTimeStatus2']);
            $output->writeln($row['TimeEntsogTestedtodeliver']);
            $output->writeln($row['Closed']);
            $output->writeln($row['ResponseTimeStatus3']);
            $output->writeln($row['TimeEntsogclosed']);
        }
        //$output->writeln(print_r($result));
        $output->writeln('######### 5 #########');
        // exit;
    }
}
