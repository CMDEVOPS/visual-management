<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Chart;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ComboChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Options\ComboChart\Series;
use CMEN\GoogleChartsBundle\GoogleCharts\Options\VAxis;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class Chart.
 */
class Chart extends Controller
{
    const ANIMATION_STARTUP = true;
    const ANIMATION_DURATION = 1000;
    const CHART_AREA_HEIGHT = '80%';
    const CHART_AREA_WIDTH = '80%';
    const COLOR_YELLOW = '#f6dc12';
    const COLOR_GREEN = '#759e1a';

    /**
     * @var ChartData
     */
    private $chartData;

    /**
     * Chart constructor.
     *
     * @param ChartData $chartData
     */
    public function __construct(ChartData $chartData)
    {
        $this->chartData = $chartData;
    }

    /**
     * Crée le graphique des rendements par mois.
     *
     * @return ComboChart
     */
    public function rendementByMonth()
    {
        $arrayToDataTable = $this->chartData->formatData();

        $chart = new ComboChart();
        $chart->getData()->setArrayToDataTable($arrayToDataTable);
        $chart->getOptions()->getAnimation()->setStartup(self::ANIMATION_STARTUP);
        $chart->getOptions()->getAnimation()->setDuration(self::ANIMATION_DURATION);
        $chart->getOptions()->getChartArea()->setHeight(self::CHART_AREA_HEIGHT);
        $chart->getOptions()->getChartArea()->setWidth(self::CHART_AREA_WIDTH);

        $vAxisAmount = new VAxis();
        $vAxisAmount->setTitle('Mois');
        $vAxisEvol = new VAxis();
        $vAxisEvol->setTitle('Rendement en %');
        $chart->getOptions()->setVAxes([$vAxisAmount, $vAxisEvol]);

        $seriesAmount = new Series();
        $seriesAmount->setType('bars');
        $seriesAmount->setTargetAxisIndex(0);
        $seriesEvol = new Series();
        $seriesEvol->setType('line');
        $seriesEvol->setTargetAxisIndex(1);

        $chart->getOptions()->setSeries([$seriesAmount, $seriesEvol]);
        $chart->getOptions()->getHAxis()->setTitle('Année');
        $chart->getOptions()->setColors([self::COLOR_YELLOW, self::COLOR_GREEN]);

        return $chart;
    }
}
