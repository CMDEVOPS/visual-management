<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Chart;

use AppBundle\Manager\ConnectionManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ChartData.
 */
class ChartData extends Controller
{
    /**
     * @var ConnectionManager
     */
    private $connectionManager;

    /**
     * Chart constructor.
     *
     * @param ConnectionManager $connectionManager
     */
    public function __construct(ConnectionManager $connectionManager)
    {
        $this->connectionManager = $connectionManager;
    }

    /**
     * @return array
     */
    public function formatData()
    {
        return $this->connectionManager->executeRendementByMonthProc();
    }
}
