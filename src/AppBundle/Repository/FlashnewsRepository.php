<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Repository;

<<<<<<< HEAD
use AppBundle\Entity\ProjectInterface;
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
use Doctrine\ORM\EntityRepository;

/**
 * Class FlashnewsRepository.
 */
class FlashnewsRepository extends EntityRepository
{
    /**
     * @param mixed $id
     *
     * @return array
     */
    public function findByProject($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f.id, f.title, f.description, f.dated, f.datef, IDENTITY(f.idproject) 
                     FROM AppBundle:Flashnews f
                     WHERE IDENTITY(f.idproject) = :id
                     AND CURRENT_TIMESTAMP() between f.dated and f.datef'
            )
            ->setParameter('id', $id)
            ->getResult();
    }
<<<<<<< HEAD

    /**
     * Return active Flash News only.
     *
     * @param ProjectInterface $project
     *
     * @return array
     *
     * @throws \Exception
     */
    public function activeFlashNews(ProjectInterface $project)
    {
        return $this->createQueryBuilder('f')
            ->select('f')
            ->where(':current_date BETWEEN f.dated AND f.datef')
            ->andWhere('f.idproject = :project')
            ->setParameter('current_date', new \DateTime('now'))
            ->setParameter('project', $project)
            ->getQuery()
            ->getResult();
    }
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
}
