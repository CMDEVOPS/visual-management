<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class IssuesRepository.
 */
class IssuesRepository extends EntityRepository
{
    /**
     * @param mixed $startDate
     * @param mixed $endDate
     *
     * @return array
     */
    public function findInRange($startDate, $endDate)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT i.id,i.projectId,i.trackerId,i.priorityId,i.createdOn,i.statusId,i.subject,i.updatedOn,i.startDate,i.remainingHours,i.estimatedHours
                     FROM AppBundle:Issues i
                     WHERE i.projectId IN (3,8) 
                     AND i.trackerId in (1,3,4,7,8,9,10)
                     AND i.createdOn BETWEEN :startDate AND :endDate'
            )
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getResult();
    }

    /**
     * @param mixed $id
     *
     * @return array
     */
    public function findStatus($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s.name
                     FROM AppBundle:IssueStatuses s
                     WHERE s.id =:id '
            )
            ->setParameter('id', $id)
            ->getResult();
    }

    /**
     * @param mixed $id
     *
     * @return array
     */
    public function findTracker($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t.name
                     FROM AppBundle:Trackers t
                     WHERE t.id =:id '
            )
            ->setParameter('id', $id)
            ->getResult();
    }

    /**
     * @param mixed $id
     * @param mixed $startDate
     * @param mixed $endDate
     *
     * @return array
     */
    public function findBySeverity($id, $startDate, $endDate)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT i.id,i.projectId,i.trackerId,i.priorityId,i.createdOn,i.statusId,i.subject,i.updatedOn,i.startDate,i.remainingHours,i.estimatedHours
                     FROM AppBundle:Issues i
                     WHERE i.projectId IN (3,4) AND i.statusId NOT IN (SELECT s.id FROM AppBundle:IssueStatuses s WHERE s.isClosed = 1 ) AND i.priorityId =:id
                     AND i.createdOn BETWEEN :startDate AND :endDate'
            )
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('id', $id)
            ->getResult();
    }

    /**
     * @param mixed $year
     * @param mixed $month
     * @param mixed $day
     *
     * @return mixed
     */
    public function findOneByYearMonthDay($year, $month, $day)
    {
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        $qb = $this->createQueryBuilder('p');
        $qb->select('p')
            ->where('YEAR(p.postDate) = :year')
            ->andWhere('MONTH(p.postDate) = :month')
            ->andWhere('DAY(p.postDate) = :day');

        $qb->setParameter('year', $year)
            ->setParameter('month', $month)
            ->setParameter('day', $day);

        $post = $qb->getQuery()->getSingleResult();

        return $post;
    }

    /**
     * @param mixed $year
     *
     * @return array
     */
    public function findByYear($year)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT MONTH(i.updatedOn) AS mois, COUNT(i.updatedOn) AS TolalTickets, COUNT(i.closedOn) AS ClosedTickets, (COUNT(i.closedOn)/COUNT(i.updatedOn))*100 AS Rendement
                     FROM AppBundle:Issues i
                     WHERE YEAR(i.updatedOn) =:year
                     GROUP BY mois
                     ORDER BY mois ASC'
            )
            ->setParameter('year', $year)
            ->getResult();
    }

    /**
     * @param mixed $month
     * @param mixed $year
     *
     * @return mixed
     */
    public function findOpenedIssues($month, $year)
    {
        $qb = $this->createQueryBuilder('i');
        $qb->select('i')
            ->innerJoin('i.statusId', 's')
            ->where('s.isClosed = false')
            ->andWhere('YEAR(i.createdOn) = :year')
            ->andWhere('MONTH(i.createdOn) = :month');

        $qb->setParameter('year', $year)
            ->setParameter('month', $month);

        $post = $qb->getQuery()->getResult();

        return $post;
    }
}
