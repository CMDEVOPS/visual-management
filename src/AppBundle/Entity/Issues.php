<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Issues.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IssuesRepository")
 * @ORM\Table(name="issues", indexes={@ORM\Index(name="issues_project_id", columns={"project_id"}), @ORM\Index(name="index_issues_on_status_id", columns={"status_id"}), @ORM\Index(name="index_issues_on_category_id", columns={"category_id"}), @ORM\Index(name="index_issues_on_assigned_to_id", columns={"assigned_to_id"}), @ORM\Index(name="index_issues_on_fixed_version_id", columns={"fixed_version_id"}), @ORM\Index(name="index_issues_on_tracker_id", columns={"tracker_id"}), @ORM\Index(name="index_issues_on_priority_id", columns={"priority_id"}), @ORM\Index(name="index_issues_on_author_id", columns={"author_id"}), @ORM\Index(name="index_issues_on_created_on", columns={"created_on"}), @ORM\Index(name="index_issues_on_root_id_and_lft_and_rgt", columns={"root_id", "lft", "rgt"}), @ORM\Index(name="index_issues_on_position", columns={"position"}), @ORM\Index(name="index_issues_on_release_id", columns={"release_id"}), @ORM\Index(name="index_issues_on_release_relationship", columns={"release_relationship"})})
 */
class Issues implements IssuesInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="tracker_id", type="integer", nullable=false)
     */
    private $trackerId;

    /**
     * @var int
     *
     * @ORM\Column(name="project_id", type="integer", nullable=false)
     */
    private $projectId;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="date", nullable=true)
     */
    private $dueDate;

    /**
     * @var int
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="status_id", type="integer", nullable=false)
     */
    private $statusId;

    /**
     * @var int
     *
     * @ORM\Column(name="assigned_to_id", type="integer", nullable=true)
     */
    private $assignedToId;

    /**
     * @var int
     *
     * @ORM\Column(name="priority_id", type="integer", nullable=false)
     */
    private $priorityId;

    /**
     * @var int
     *
     * @ORM\Column(name="fixed_version_id", type="integer", nullable=true)
     */
    private $fixedVersionId;

    /**
     * @var int
     *
     * @ORM\Column(name="author_id", type="integer", nullable=false)
     */
    private $authorId;

    /**
     * @var int
     *
     * @ORM\Column(name="lock_version", type="integer", nullable=false)
     */
    private $lockVersion = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=true)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=true)
     */
    private $updatedOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var int
     *
     * @ORM\Column(name="done_ratio", type="integer", nullable=false)
     */
    private $doneRatio = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="estimated_hours", type="float", precision=10, scale=0, nullable=true)
     */
    private $estimatedHours;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="root_id", type="integer", nullable=true)
     */
    private $rootId;

    /**
     * @var int
     *
     * @ORM\Column(name="lft", type="integer", nullable=true)
     */
    private $lft;

    /**
     * @var int
     *
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closed_on", type="datetime", nullable=true)
     */
    private $closedOn;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var float
     *
     * @ORM\Column(name="remaining_hours", type="float", precision=10, scale=0, nullable=true)
     */
    private $remainingHours;

    /**
     * @var int
     *
     * @ORM\Column(name="release_id", type="integer", nullable=true)
     */
    private $releaseId;

    /**
     * @var float
     *
     * @ORM\Column(name="story_points", type="float", precision=10, scale=0, nullable=true)
     */
    private $storyPoints;

    /**
     * @var string
     *
     * @ORM\Column(name="release_relationship", type="string", length=255, nullable=false)
     */
    private $releaseRelationship = 'auto';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return int
     */
    public function getAssignedToId()
    {
        return $this->assignedToId;
    }

    /**
     * @param int $assignedToId
     */
    public function setAssignedToId($assignedToId)
    {
        $this->assignedToId = $assignedToId;
    }

    /**
     * @return int
     */
    public function getPriorityId()
    {
        return $this->priorityId;
    }

    /**
     * @param int $priorityId
     */
    public function setPriorityId($priorityId)
    {
        $this->priorityId = $priorityId;
    }

    /**
     * @return int
     */
    public function getFixedVersionId()
    {
        return $this->fixedVersionId;
    }

    /**
     * @param int $fixedVersionId
     */
    public function setFixedVersionId($fixedVersionId)
    {
        $this->fixedVersionId = $fixedVersionId;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return int
     */
    public function getLockVersion()
    {
        return $this->lockVersion;
    }

    /**
     * @param int $lockVersion
     */
    public function setLockVersion($lockVersion)
    {
        $this->lockVersion = $lockVersion;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return int
     */
    public function getDoneRatio()
    {
        return $this->doneRatio;
    }

    /**
     * @param int $doneRatio
     */
    public function setDoneRatio($doneRatio)
    {
        $this->doneRatio = $doneRatio;
    }

    /**
     * @return float
     */
    public function getEstimatedHours()
    {
        return $this->estimatedHours;
    }

    /**
     * @param float $estimatedHours
     */
    public function setEstimatedHours($estimatedHours)
    {
        $this->estimatedHours = $estimatedHours;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * @param int $rootId
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;
    }

    /**
     * @return int
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param int $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return int
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param int $rgt
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * @param bool $isPrivate
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;
    }

    /**
     * @return \DateTime
     */
    public function getClosedOn()
    {
        return $this->closedOn;
    }

    /**
     * @param \DateTime $closedOn
     */
    public function setClosedOn($closedOn)
    {
        $this->closedOn = $closedOn;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return float
     */
    public function getRemainingHours()
    {
        return $this->remainingHours;
    }

    /**
     * @param float $remainingHours
     */
    public function setRemainingHours($remainingHours)
    {
        $this->remainingHours = $remainingHours;
    }

    /**
     * @return int
     */
    public function getReleaseId()
    {
        return $this->releaseId;
    }

    /**
     * @param int $releaseId
     */
    public function setReleaseId($releaseId)
    {
        $this->releaseId = $releaseId;
    }

    /**
     * @return float
     */
    public function getStoryPoints()
    {
        return $this->storyPoints;
    }

    /**
     * @param float $storyPoints
     */
    public function setStoryPoints($storyPoints)
    {
        $this->storyPoints = $storyPoints;
    }

    /**
     * @return string
     */
    public function getReleaseRelationship()
    {
        return $this->releaseRelationship;
    }

    /**
     * @param string $releaseRelationship
     */
    public function setReleaseRelationship($releaseRelationship)
    {
        $this->releaseRelationship = $releaseRelationship;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * @param int $trackerId
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param int $projectId
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    }
}
