<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

<<<<<<< HEAD
use Doctrine\Common\Collections\ArrayCollection;
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
use Doctrine\ORM\Mapping as ORM;

/**
 * Project.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\Table(name="project")
 */
class Project implements ProjectInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
<<<<<<< HEAD
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Users",mappedBy="project")
     */
    private $users;
=======
     * @ORM\OneToMany(targetEntity="Users",mappedBy="project")
     */
    private $user;
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
<<<<<<< HEAD
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @{inheritdoc}
=======
     * @return int
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
<<<<<<< HEAD
     * Add user.
     *
     * @param UsersInterface $user
     *
     * @return Project
     */
    public function addUser(UsersInterface $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param UsersInterface $user
     */
    public function removeUser(UsersInterface $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
=======
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
    }
}
