<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

/**
 * interface IssuesInterface.
 */
interface IssuesInterface
{
    /**
     * @return int
     */
    public function getCategoryId();

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId);

    /**
     * @return int
     */
    public function getStatusId();

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId);

    /**
     * @return int
     */
    public function getAssignedToId();

    /**
     * @param int $assignedToId
     */
    public function setAssignedToId($assignedToId);

    /**
     * @return int
     */
    public function getPriorityId();

    /**
     * @param int $priorityId
     */
    public function setPriorityId($priorityId);

    /**
     * @return int
     */
    public function getFixedVersionId();

    /**
     * @param int $fixedVersionId
     */
    public function setFixedVersionId($fixedVersionId);

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId);

    /**
     * @return int
     */
    public function getLockVersion();

    /**
     * @param int $lockVersion
     */
    public function setLockVersion($lockVersion);

    /**
     * @return \DateTime
     */
    public function getCreatedOn();

    /**
     * @param \DateTime $createdOn
     */
    public function setCreatedOn($createdOn);

    /**
     * @return \DateTime
     */
    public function getUpdatedOn();

    /**
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn($updatedOn);

    /**
     * @return \DateTime
     */
    public function getStartDate();

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate);

    /**
     * @return int
     */
    public function getDoneRatio();

    /**
     * @param int $doneRatio
     */
    public function setDoneRatio($doneRatio);

    /**
     * @return float
     */
    public function getEstimatedHours();

    /**
     * @param float $estimatedHours
     */
    public function setEstimatedHours($estimatedHours);

    /**
     * @return int
     */
    public function getParentId();

    /**
     * @param int $parentId
     */
    public function setParentId($parentId);

    /**
     * @return int
     */
    public function getRootId();

    /**
     * @param int $rootId
     */
    public function setRootId($rootId);

    /**
     * @return int
     */
    public function getLft();

    /**
     * @param int $lft
     */
    public function setLft($lft);

    /**
     * @return int
     */
    public function getRgt();

    /**
     * @param int $rgt
     */
    public function setRgt($rgt);

    /**
     * @return bool
     */
    public function isPrivate();

    /**
     * @param bool $isPrivate
     */
    public function setIsPrivate($isPrivate);

    /**
     * @return \DateTime
     */
    public function getClosedOn();

    /**
     * @param \DateTime $closedOn
     */
    public function setClosedOn($closedOn);

    /**
     * @return int
     */
    public function getPosition();

    /**
     * @param int $position
     */
    public function setPosition($position);

    /**
     * @return float
     */
    public function getRemainingHours();

    /**
     * @param float $remainingHours
     */
    public function setRemainingHours($remainingHours);

    /**
     * @return int
     */
    public function getReleaseId();

    /**
     * @param int $releaseId
     */
    public function setReleaseId($releaseId);

    /**
     * @return float
     */
    public function getStoryPoints();

    /**
     * @param float $storyPoints
     */
    public function setStoryPoints($storyPoints);

    /**
     * @return string
     */
    public function getReleaseRelationship();

    /**
     * @param string $releaseRelationship
     */
    public function setReleaseRelationship($releaseRelationship);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getTrackerId();

    /**
     * @param int $trackerId
     */
    public function setTrackerId($trackerId);

    /**
     * @return int
     */
    public function getProjectId();

    /**
     * @param int $projectId
     */
    public function setProjectId($projectId);

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @param string $subject
     */
    public function setSubject($subject);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     */
    public function setDescription($description);

    /**
     * @return \DateTime
     */
    public function getDueDate();

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate($dueDate);
}
