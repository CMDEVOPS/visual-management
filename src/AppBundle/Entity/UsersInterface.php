<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

/**
 * interface UsersInterface.
 */
interface UsersInterface
{
    /**
     * @return string
     */
    public function getTrigramme();

    /**
     * @param string $trigramme
     */
    public function setTrigramme($trigramme);

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @param string $password
     */
    public function setPassword($password);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
<<<<<<< HEAD
     * @return ProjectInterface
=======
     * @return mixed
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     */
    public function getProject();

    /**
<<<<<<< HEAD
     * @param ProjectInterface $project
=======
     * @param mixed $project
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     */
    public function setProject($project);

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName);

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @param string $lastName
     */
    public function setLastName($lastName);
}
