<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

/**
 * interface SlaStandardsInterface.
 */
interface SlaStandardsInterface
{
    /**
     * @return string
     */
    public function getTicketType();

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType);

    /**
     * @return string
     */
    public function getTicketCriticality();

    /**
     * @param string $ticketCriticality
     */
    public function setTicketCriticality($ticketCriticality);

    /**
     * @return float
     */
    public function getAnswer();

    /**
     * @param float $answer
     */
    public function setAnswer($answer);

    /**
     * @return float
     */
    public function getResolution();

    /**
     * @param float $resolution
     */
    public function setResolution($resolution);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return Project
     */
    public function getIdProject();

    /**
     * @param Project $idProject
     */
    public function setIdProject($idProject);
}
