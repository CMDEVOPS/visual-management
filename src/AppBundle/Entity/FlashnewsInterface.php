<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

/**
 * interface FlashnewsInterface.
 */
interface FlashnewsInterface
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     */
    public function setDescription($description);

    /**
     * @return \DateTime
     */
    public function getDated();

    /**
     * @param \DateTime $dated
     */
    public function setDated($dated);

    /**
     * @return \DateTime
     */
    public function getDatef();

    /**
     * @param \DateTime $datef
     */
    public function setDatef($datef);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     */
    public function setId($id);

    /**
     * @return Project
     */
    public function getIdproject();

    /**
     * @param Project $idproject
     */
    public function setIdproject($idproject);
}
