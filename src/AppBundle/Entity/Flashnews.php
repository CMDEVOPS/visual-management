<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flashnews.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FlashnewsRepository")
 * @ORM\Table(name="flashNews")
 */
class Flashnews implements FlashnewsInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=250, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateD", type="datetime", nullable=false)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateF", type="datetime", nullable=false)
     */
    private $datef;

    /**
     * @var \AppBundle\Entity\Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_project", referencedColumnName="id")
     * })
     */
    private $idproject;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * @param \DateTime $dated
     */
    public function setDated($dated)
    {
        $this->dated = $dated;
    }

    /**
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * @param \DateTime $datef
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getIdproject()
    {
        return $this->idproject;
    }

    /**
     * @param Project $idproject
     */
    public function setIdproject($idproject)
    {
        $this->idproject = $idproject;
    }
}
