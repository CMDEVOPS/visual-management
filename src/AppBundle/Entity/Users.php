<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Users.
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsersRepository")
 */
class Users extends BaseUser implements UsersInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="trigramme", type="string", length=4, nullable=false)
     */
    private $trigramme;

    /**
<<<<<<< HEAD
     * @var ProjectInterface
     *
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     * @ORM\ManyToOne(targetEntity="Project",inversedBy="user")
     * @ORM\JoinColumn(name="id_project",referencedColumnName="id")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=250, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=250, nullable=false)
     */
    private $lastName;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @return string
     */
    public function getTrigramme()
    {
        return $this->trigramme;
    }

    /**
     * @param string $trigramme
     */
    public function setTrigramme($trigramme)
    {
        $this->trigramme = $trigramme;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
<<<<<<< HEAD
     * @return ProjectInterface
=======
     * @return mixed
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
<<<<<<< HEAD
     * @param ProjectInterface $project
=======
     * @param mixed $project
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
}
