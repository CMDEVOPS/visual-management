<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SlaStandards.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SlaStandardsRepository")
 * @ORM\Table(name="SLA_standards", indexes={@ORM\Index(name="IDX_5F4FC657F12E799E", columns={"id_project"})})
 */
class SlaStandards implements SlaStandardsInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="ticket_type", type="string", length=255, nullable=false)
     */
    private $ticketType;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_criticality", type="string", length=255, nullable=false)
     */
    private $ticketCriticality;

    /**
     * @var float
     *
     * @ORM\Column(name="answer", type="float", precision=10, scale=0, nullable=false)
     */
    private $answer;

    /**
     * @var float
     *
     * @ORM\Column(name="resolution", type="float", precision=10, scale=0, nullable=false)
     */
    private $resolution;

    /**
     * @var \AppBundle\Entity\Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_project", referencedColumnName="id")
     * })
     */
    private $idProject;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return string
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
    }

    /**
     * @return string
     */
    public function getTicketCriticality()
    {
        return $this->ticketCriticality;
    }

    /**
     * @param string $ticketCriticality
     */
    public function setTicketCriticality($ticketCriticality)
    {
        $this->ticketCriticality = $ticketCriticality;
    }

    /**
     * @return float
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param float $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return float
     */
    public function getResolution()
    {
        return $this->resolution;
    }

    /**
     * @param float $resolution
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getIdProject()
    {
        return $this->idProject;
    }

    /**
     * @param Project $idProject
     */
    public function setIdProject($idProject)
    {
        $this->idProject = $idProject;
    }
}
