<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Manager;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntsogManager.
 */
class EntsogManager
{
    /**
     * @var EntityManager
     */
    private $defaultManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * EntsogManager constructor.
     *
     * @param ContainerInterface $container
     * @param LoggerInterface    $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {
        $this->defaultManager = $container->get('doctrine')->getManager('default');
        $this->logger = $logger;
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function executeVisualProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Visual_management($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function executeCustomVisualProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Custom_visual_management($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @param int $currentYear
     *
     * @return array
     */
    public function executeSlaDetailledProc($currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL SLA_Detailled('2',$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @return array
     */
    public function executeRendementByMonthProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Rendement_By_Month()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @return array
     */
    public function executeTicketsStatusProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Assignee_Tickets_Status()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function executeTicketsPriorityProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Assignee_Tickets_Priority($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @return array
     */
    public function executeTicketsByRessourcesProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Tickets_By_Ressources()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @return array
     */
    public function executeTreatedTicketsByRessourcesProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL treatedTickets_By_Ressources()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }
}
