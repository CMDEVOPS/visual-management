<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Manager;

<<<<<<< HEAD
use AppBundle\Entity\Flashnews;
use AppBundle\Entity\ProjectInterface;
use AppBundle\Entity\SlaStandards;
use AppBundle\Entity\SlaStandardsInterface;
use AppBundle\Entity\UsersInterface;
use AppBundle\Utils\Calculator;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
=======
use AppBundle\Entity\SlaStandards;
use AppBundle\Entity\SlaStandardsInterface;
use AppBundle\Utils\Calculator;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

/**
 * Class ConnectionManager.
 */
class ConnectionManager
{
    /**
     * @var EntityManager
     */
    private $defaultManager;

    /**
     * @var EntityManager
     */
    private $customerManager;

    /**
<<<<<<< HEAD
     * @var EntsogManager
     */
    private $entsogManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     * @var Calculator
     */
    private $calculator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
<<<<<<< HEAD
     * ConnectionManager constructor.
     *
     * @param ContainerInterface    $container
     * @param EntsogManager         $entsogManager
     * @param TokenStorageInterface $tokenStorage
     * @param Calculator            $calculator
     * @param LoggerInterface       $logger
     */
    public function __construct(ContainerInterface $container, EntsogManager $entsogManager, TokenStorageInterface $tokenStorage, Calculator $calculator, LoggerInterface $logger)
    {
        $this->defaultManager = $container->get('doctrine')->getManager('default');
        $this->customerManager = $container->get('doctrine')->getManager('customer');
        $this->entsogManager = $entsogManager;
        $this->tokenStorage = $tokenStorage;
=======
     * DefaultController constructor.
     *
     * @param ContainerInterface $container
     * @param Calculator         $calculator
     * @param LoggerInterface    $logger
     */
    public function __construct(ContainerInterface $container, Calculator $calculator, LoggerInterface $logger)
    {
        $this->defaultManager = $container->get('doctrine')->getManager('default');
        $this->customerManager = $container->get('doctrine')->getManager('customer');
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        $this->calculator = $calculator;
        $this->logger = $logger;
    }

    /**
     * @return EntityManager
     */
    public function getDefaultManager()
    {
        return $this->defaultManager;
    }

    /**
     * @return EntityManager
     */
    public function getCustomerManager()
    {
        return $this->customerManager;
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function issueStatusOpened($currentMonth, $currentYear)
    {
<<<<<<< HEAD
        $issuesToHandle = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $issuesToHandle = $this->entsogManager->executeCustomVisualProc($currentMonth, $currentYear);
                $issuesToHandle = $this->calculate($issuesToHandle);
                break;
        }

        return $issuesToHandle;
=======
        $issuesToHandle = $this->executeCustomVisualProc($currentMonth, $currentYear);

        return $this->calculate($issuesToHandle);
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function issueStatus($currentMonth, $currentYear)
    {
<<<<<<< HEAD
        $issuesToHandle = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $issuesToHandle = $this->entsogManager->executeVisualProc($currentMonth, $currentYear);
                $issuesToHandle = $this->calculate($issuesToHandle);
                break;
        }

        return $issuesToHandle;
    }

    /**
=======
        $issuesToHandle = $this->executeVisualProc($currentMonth, $currentYear);

        return $this->calculate($issuesToHandle);
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
    public function executeVisualProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Visual_management($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @param int $currentMonth
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     * @param int $currentYear
     *
     * @return array
     */
<<<<<<< HEAD
    public function slaDetailled($currentYear)
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeSlaDetailledProc($currentYear);
                break;
=======
    public function executeCustomVisualProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Custom_visual_management($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
<<<<<<< HEAD
     * @return array
     */
    public function rendementByMonth()
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeRendementByMonthProc();
                break;
=======
     * @param int $currentYear
     *
     * @return array
     */
    public function executeSLAProc($currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL SLA_Detailled('2',$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
     * @return array
     */
<<<<<<< HEAD
    public function assigneeTicketsStatus()
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeTicketsStatusProc();
                break;
=======
    public function executeRendementByMonthProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Rendement_By_Month()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }

    /**
     * @return array
     */
    public function executeTicketsStatusProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Assignee_Tickets_Status()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     *
     * @return array
     */
<<<<<<< HEAD
    public function assigneeTicketsPriority($currentMonth, $currentYear)
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeTicketsPriorityProc($currentMonth, $currentYear);
                break;
=======
    public function executeTicketsPriorityProc($currentMonth, $currentYear)
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare("CALL Assignee_Tickets_Priority($currentMonth ,$currentYear)");
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
     * @return array
     */
<<<<<<< HEAD
    public function ticketsByRessources()
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeTicketsByRessourcesProc();
                break;
=======
    public function executeTicketsByRessourcesProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL Tickets_By_Ressources()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
     * @return array
     */
<<<<<<< HEAD
    public function treatedTicketsByRessources()
    {
        $results = [];
        switch ($this->getUserProject()) {
            case ProjectInterface::ENTSOG:
                $results = $this->entsogManager->executeTreatedTicketsByRessourcesProc();
                break;
=======
    public function executeTreatedTicketsByRessourcesProc()
    {
        $results = [];
        try {
            $sth = $this->defaultManager->getConnection()->prepare('CALL treatedTickets_By_Ressources()');
            $sth->execute();
            $results = $sth->fetchAll();
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage());
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
        }

        return $results;
    }

    /**
<<<<<<< HEAD
     * @param int $month
     * @param int $year
=======
     * @param int $year
     * @param int $month
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     *
     * @return mixed
     */
    public function findOpenedIssues($month, $year)
    {
        $qb = $this->getDefaultManager()->getConnection()->createQueryBuilder();
        $qb->select('i.*, s.name as status, t.name as Tracker, p.name as project, e.name as Severity')
            ->from('issues', 'i')
            ->leftJoin('i', 'issue_statuses', 's', 'i.status_id = s.id')
            ->leftJoin('i', 'projects', 'p', 'i.project_id = p.id')
            ->leftJoin('i', 'trackers', 't', 'i.tracker_id = t.id')
            ->leftJoin('i', 'enumerations', 'e', 'i.priority_id = e.id')
            ->where('s.is_closed = false')
            ->andWhere('YEAR(i.created_on) = :year')
            ->andWhere('MONTH(i.created_on) = :month');

        $qb->setParameter('year', $year)
            ->setParameter('month', $month);

        $post = $qb->execute()->fetchAll();

        return $post;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findStatusIssues($id)
    {
        $qb = $this->getDefaultManager()->getConnection()->createQueryBuilder();
        $qb->select('s.name as status, s.is_closed as closed')
            ->from('issues', 'i')
            ->leftJoin('i', 'issue_statuses', 's', 'i.status_id = s.id')
            ->where('i.id = :id');

        $qb->setParameter('id', $id);

        $post = $qb->execute()->fetch();

        return $post;
    }

    /**
<<<<<<< HEAD
     * @return array
     */
    public function findActiveFlashNews()
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if ($user instanceof UsersInterface && $user->getProject() instanceof ProjectInterface) {
            return $this->customerManager->getRepository(Flashnews::class)->activeFlashNews($user->getProject());
        }

        return [];
    }

    /**
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     * @param array $issuesToHandle
     *
     * @return array
     */
    private function calculate(array $issuesToHandle)
    {
        $resultIssuesToHandle = [];

        foreach ($issuesToHandle as $row) {
            switch ($row['Tracker']) {
<<<<<<< HEAD
                case IssueManager::BUG_TRACKER:
                    $row['bugTracker'] = 'Bugs';
                    break;
                case IssueManager::STORY_TRACKER:
                    $row['bugTracker'] = 'Story';
                    break;
                case IssueManager::ANALYSIS_TRACKER:
                    $row['bugTracker'] = 'AR';
                    break;
                case IssueManager::SUPPORT_TRACKER || IssueManager::PROTOCOL_TRACKER:
=======
                case 'Bug PRD':
                    $row['bugTracker'] = 'Bugs';
                    break;
                case 'Story':
                    $row['bugTracker'] = 'Story';
                    break;
                case 'Analysis Request':
                    $row['bugTracker'] = 'AR';
                    break;
                case 'Support':
                    $row['bugTracker'] = 'SR';
                    break;
                case 'Protocol Request':
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
                    $row['bugTracker'] = 'SR';
                    break;
                default:
                    $row['bugTracker'] = '';
            }
            $type = $this->customerManager->getRepository(SlaStandards::class)->findOneBy([
                'ticketType' => $row['bugTracker'],
                'ticketCriticality' => $row['Severity'],
            ]);

<<<<<<< HEAD
            if ($type instanceof SlaStandardsInterface) {
=======
            if ($type instanceof  SlaStandardsInterface) {
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
                $resultIssuesToHandle[] = $this->calculator->calculateSlaTime($row, $type->getResolution());
            }
        }

        return $resultIssuesToHandle;
    }
<<<<<<< HEAD

    /**
     * @return string
     */
    private function getUserProject()
    {
        $project = '';
        $user = $this->tokenStorage->getToken()->getUser();

        if ($user instanceof UsersInterface) {
            $project = $user->getProject()->getLabel();
        }

        return $project;
    }
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
}
