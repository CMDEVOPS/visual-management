<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Mathieu Palouki <spalouki@umanis.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Manager;

/**
 * Class IssueManager.
 */
class IssueManager
{
    const SLA_STARTING_DAY = '14';
    const SLA_STARTING_MONTH = '03';
    const SLA_STARTING_YEAR = '2017';
<<<<<<< HEAD
    const BUG_TRACKER = 'Bug PRD';
    const STORY_TRACKER = 'Story';
    const ANALYSIS_TRACKER = 'Analysis Request';
    const SUPPORT_TRACKER = 'Support';
    const PROTOCOL_TRACKER = 'Protocol Request';
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1

    /**
     * @return string
     */
    public static function getStartingDate()
    {
        return self::SLA_STARTING_YEAR.'-'.self::SLA_STARTING_MONTH.'-'.self::SLA_STARTING_DAY;
    }

    /**
     * @param array $issuesToHandle
     *
     * @return array
     */
    public static function getMonthNameByNumber(array $issuesToHandle)
    {
        $months = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $resultIssuesToHandle = [];

        foreach ($issuesToHandle as $row) {
            $row['mois'] = $months[$row['mois'] - 1];
            $resultIssuesToHandle[] = $row;
        }

        return $resultIssuesToHandle;
    }

    /**
<<<<<<< HEAD
     * @param array $result
     *
     * @return array
     */
    public static function countIssuesByTracker(array $result)
    {
        $bugs = $stories = $AR = $SR = 0;

        foreach ($result as $row) {
            switch ($row['Tracker']) {
                case self::BUG_TRACKER:
                    $bugs++;
                    break;
                case self::STORY_TRACKER:
                    $stories++;
                    break;
                case self::ANALYSIS_TRACKER:
                    $AR++;
                    break;
                case self::SUPPORT_TRACKER || self::PROTOCOL_TRACKER:
                    $SR++;
                    break;
            }
        }

        return [
            'bugs' => $bugs,
            'stories' => $stories,
            'analysis' => $AR,
            'supports' => $SR,
        ];
    }

    /**
     * @param string $tracker
     *
     * @return string
     */
    public static function resolveTracker($tracker)
    {
        $resolve = '';
        switch ($tracker) {
            case self::BUG_TRACKER:
                $resolve = 'bug';
                break;
            case self::STORY_TRACKER:
                $resolve = 'story';
                break;
            case self::ANALYSIS_TRACKER:
                $resolve = 'analysis';
                break;
            case self::SUPPORT_TRACKER || self::PROTOCOL_TRACKER:
                $resolve = 'support';
                break;
        }

        return $resolve;
    }

    /**
=======
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
     * @param string $firstTime
     * @param string $lastTime
     *
     * @return int
     */
    public static function compareTime($firstTime, $lastTime)
    {
        $first = explode(':', $firstTime);
        $last = explode(':', $lastTime);

        if (count($first) < 3 || count($last) < 3) {
<<<<<<< HEAD
=======

>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
            return null;
        }

        foreach ($first as $key => $value) {
            if ((int) $value > (int) $last[$key]) {
<<<<<<< HEAD
                return 1;
            }
            if ((int) $value < (int) $last[$key]) {
=======

                return 1;
            } elseif ((int) $value < (int) $last[$key]) {

>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
                return -1;
            }
        }

        return 0;
    }

    /**
     * @param array $result
     *
     * @return array
     */
    public function sortIssuesBySeverity(array $result)
    {
        $issues = ['minors' => [], 'majors' => [], 'criticals' => []];

        foreach ($result as $row) {
            if ($row['Severity'] === 'Minor') {
                $issues['minors'][] = $row;
            } else {
                $row['Severity'] === 'Major' ? $issues['majors'][] = $row : $issues['criticals'][] = $row;
            }
        }

        return $issues;
    }

    /**
     * @param array $result
     *
     * @return array
     */
    public function countIssuesBySeverity(array $result)
    {
        $minors = $majors = $criticals = 0;

        foreach ($result as $row) {
            switch ($row['Severity']) {
                case 'Minor':
                    $minors++;
                    break;
                case 'Major':
                    $majors++;
                    break;
                default:
                    $criticals++;
                    break;
            }
        }

        return [
            'minors' => $minors,
            'majors' => $majors,
            'criticals' => $criticals,
        ];
    }

    /**
     * @param array $criticals
     *
     * @return array
     */
    public function sortIssuesByCriticity(array $criticals)
    {
        $issues = ['red' => [], 'yellow' => [], 'blue' => []];
        $bugs = $SR = $AR = $story = [];

        foreach ($criticals as $row) {
            switch ($row['Tracker']) {
<<<<<<< HEAD
                case self::BUG_TRACKER:
                    $bugs[] = $row;
                    break;
                case self::STORY_TRACKER:
                    $story[] = $row;
                    break;
                case self::ANALYSIS_TRACKER:
                    $AR[] = $row;
                    break;
                case self::SUPPORT_TRACKER || self::PROTOCOL_TRACKER:
=======
                case 'Bug PRD':
                    $bugs[] = $row;
                    break;
                case 'Story':
                    $story[] = $row;
                    break;
                case 'Analysis Request':
                    $AR[] = $row;
                    break;
                case 'Support':
                    $SR[] = $row;
                    break;
                case 'Protocol Request':
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
                    $SR[] = $row;
                    break;
            }
        }
        $this->sortCriticalIssues($bugs, $story, $AR, $SR, $issues);

        return $issues;
    }

    /**
     * @param array $bugs
     * @param array $story
     * @param array $AR
     * @param array $SR
     * @param array $issues
     */
    private function sortCriticalIssues(array $bugs, array $story, array $AR, array $SR, array &$issues)
    {
        $this->sortBySpentTime($bugs, '00:00:00', '01:00:00', $issues);
        $this->sortBySpentTime($story, '00:00:00', '30:00:00', $issues);
        $this->sortBySpentTime($SR, '00:00:00', '04:48:00', $issues);
        $this->sortBySpentTime($AR, '00:00:00', '30:00:00', $issues);
    }

    /**
     * @param array  $criticals
     * @param string $firstTime
     * @param string $lastTime
     * @param array  $issues
     */
    private function sortBySpentTime(array $criticals, $firstTime, $lastTime, array &$issues)
    {
        foreach ($criticals as $row) {
            if ($row['slaAbove'] > $firstTime) {
                $issues['red'][] = $row;
            } else {
                $row['spentTime'] <= $lastTime ? $issues['yellow'][] = $row : $issues['blue'][] = $row;
            }
        }
    }
}
