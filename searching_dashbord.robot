*** Settings ***
Force Tags        run5
Library           SeleniumLibrary

*** Variables ***
${link}           http://visual.management.loc/app.php/
@{date}           1/2018    2/2018    1/2019

*** Test Cases ***
calendar
    [Tags]    run1
    SeleniumLibrary.Open Browser    ${link}    Chrome
    Input Text    name:_username    entsog
    Input Password    _password    entsog93
    Click Button    xpath=//div[@class='panel-body']//button[@class='btn btn-lg btn-success btn-block']
    Click Element    xpath=//i[@class='fa fa-calendar']
    ${value}=    Evaluate    random.choice(${date})    random
    Input Text    xpath=//input[@name='calendarpicker']    ${value}
    Click Button    id=validBtn
    Element Text Should Be    xpath=//div[@class='panel-heading']//b[contains(text(),'Reporting on SLAs of the month ${value}')]    Reporting on SLAs of the month ${value}

search
    [Tags]
    Input Text    //input[@type='search']    Support
    ${count}=    Get Element Count    //table[@id='dataTables-example']//tbody//tr
    : FOR    ${INDEX}    IN RANGE    2    ${count}
    \    Table Cell Should Contain    //table[@id='dataTables-example']    ${INDEX}    3    Support
