<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: spk
 * Date: 17/01/2019
 * Time: 13:24
 */

namespace Tests\AppBundle\Manager;

use AppBundle\Manager\IssueManager;
use PHPUnit\Framework\TestCase;

class IssueManagerTest extends TestCase
{
    /** @var IssueManager */
    protected $issueManager;

    protected function setUp()
    {
        $this->issueManager = new IssueManager();
    }

    public function testGetStartingDate()
    {
        $this->assertSame('2017-03-14', IssueManager::getStartingDate());
    }

    /**
     * @dataProvider getMonths
     */
    public function testGetMonthNameByNumber(array $row, array $expectation)
    {
        $result = IssueManager::getMonthNameByNumber($row);

        foreach ($expectation as $key => $expected) {
            $this->assertSame($expected, $result[$key]['mois']);
        }
    }

    /**
     * @dataProvider getIssuesTracker
     */
    public function testCountIssuesByTracker(array $issues, array $expectation)
    {
        $result = $this->issueManager->countIssuesByTracker($issues);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, $result[$key]);
        }
    }

    /**
     * @dataProvider getResolveTracker
     */
    public function testResolveTracker($tracker, $expectation)
    {
        $resolve = $this->issueManager->resolveTracker($tracker);

        $this->assertEquals($expectation, $resolve);
    }

    /**
     * @dataProvider getIssues
     */
    public function testSortIssuesBySeverity(array $issues, array $expectation)
    {
        $result = $this->issueManager->sortIssuesBySeverity($issues);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, count($result[$key]));
        }
    }

    /**
     * @dataProvider getIssues
     */
    public function testCountIssuesBySeverity(array $issues, array $expectation)
    {
        $result = $this->issueManager->countIssuesBySeverity($issues);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, $result[$key]);
        }
    }

    /**
     * @dataProvider getCriticalIssues
     */
    public function testSortIssuesByCriticity(array $criticals, array $expectation)
    {
        $result = $this->issueManager->sortIssuesByCriticity($criticals);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, count($result[$key]));
        }
    }

    public function getMonths()
    {
        yield [
            [
                ['mois' => 12],
                ['mois' => 5],
                ['mois' => 9],
                ['mois' => 3],
                ['mois' => 7],
            ],
            [
                'December',
                'May',
                'September',
                'March',
                'July',
            ],
        ];
    }

    public function getIssues()
    {
        yield [
            [
                ['Severity' => 'Minor'],
                ['Severity' => 'Major'],
                ['Severity' => 'Critical'],
                ['Severity' => 'Minor'],
                ['Severity' => 'Major'],
            ],
            [
                'minors' => 2,
                'majors' => 2,
                'criticals' => 1,
            ],
        ];
    }

    public function getIssuesTracker()
    {
        yield [
            [
                ['Tracker' => 'Bug PRD'],
                ['Tracker' => 'Support'],
                ['Tracker' => 'Story'],
                ['Tracker' => 'Analysis Request'],
                ['Tracker' => 'Bug PRD'],
                ['Tracker' => 'Support'],
                ['Tracker' => 'Protocol Request'],
                ['Tracker' => 'Analysis Request'],
            ],
            [
                'bugs' => 2,
                'stories' => 1,
                'analysis' => 2,
                'supports' => 3,
            ],
        ];
    }

    public function getResolveTracker()
    {
        yield ['Bug PRD', 'bug'];
        yield ['Support', 'support'];
        yield ['Story', 'story'];
        yield ['Analysis Request', 'analysis'];
        yield ['Protocol Request', 'support'];
    }

    public function getCriticalIssues()
    {
        yield [
            [
                ['Severity' => 'Critical', 'Tracker' => 'Bug PRD', 'slaAbove' => 1, 'spentTime' => '6:28:17'],
                ['Severity' => 'Critical', 'Tracker' => 'Bug PRD', 'slaAbove' => 89, 'spentTime' => '94:2:50'],
                ['Severity' => 'Critical', 'Tracker' => 'Support', 'slaAbove' => 53, 'spentTime' => '77:2:0'],
                ['Severity' => 'Critical', 'Tracker' => 'Story', 'slaAbove' => 0, 'spentTime' => '16:28:17'],
                ['Severity' => 'Critical', 'Tracker' => 'Protocol Request', 'slaAbove' => 0, 'spentTime' => '13:8:17'],
            ],
            [
                'red' => 3,
                'yellow' => 1,
                'blue' => 1,
            ],
        ];
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: spk
 * Date: 17/01/2019
 * Time: 13:24
 */

namespace Tests\AppBundle\Manager;

use AppBundle\Manager\IssueManager;
use PHPUnit\Framework\TestCase;

class IssueManagerTest extends TestCase
{
    protected $issueManager;

    protected function setUp()
    {
        $this->issueManager = new IssueManager();
    }

    public function testGetStartingDate()
    {
        $this->assertSame('2017-03-14', IssueManager::getStartingDate());
    }

    /**
     * @dataProvider getMonths
     */
    public function testGetMonthNameByNumber(array $row, array $expectation)
    {
        $result = IssueManager::getMonthNameByNumber($row);

        foreach ($expectation as $key => $expected) {
            $this->assertSame($expected, $result[$key]['mois']);
        }
    }

    /**
     * @dataProvider getIssues
     */
    public function testSortIssuesBySeverity(array $issues, array $expectation)
    {
        $result = $this->issueManager->sortIssuesBySeverity($issues);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, count($result[$key]));
        }
    }

    /**
     * @dataProvider getIssues
     */
    public function testCountIssuesBySeverity(array $issues, array $expectation)
    {
        $result = $this->issueManager->countIssuesBySeverity($issues);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, $result[$key]);
        }
    }

    /**
     * @dataProvider getCriticalIssues
     */
    public function testSortIssuesByCriticity(array $criticals, array $expectation)
    {
        $result = $this->issueManager->sortIssuesByCriticity($criticals);

        foreach ($expectation as $key => $expected) {
            $this->assertEquals($expected, count($result[$key]));
        }
    }

    public function getMonths()
    {
        yield [
            [
                ['mois' => 12],
                ['mois' => 5],
                ['mois' => 9],
                ['mois' => 3],
                ['mois' => 7],
            ],
            [
                'December',
                'May',
                'September',
                'March',
                'July',
            ],
        ];
    }

    public function getIssues()
    {
        yield [
            [
                ['Severity' => 'Minor'],
                ['Severity' => 'Major'],
                ['Severity' => 'Critical'],
                ['Severity' => 'Minor'],
                ['Severity' => 'Major'],
            ],
            [
                'minors' => 2,
                'majors' => 2,
                'criticals' => 1,
            ],
        ];
    }

    public function getCriticalIssues()
    {
        yield [
            [
                ['Severity' => 'Critical', 'Tracker' => 'Bug PRD', 'slaAbove' => 1, 'spentTime' => '6:28:17'],
                ['Severity' => 'Critical', 'Tracker' => 'Bug PRD', 'slaAbove' => 89, 'spentTime' => '94:2:50'],
                ['Severity' => 'Critical', 'Tracker' => 'Support', 'slaAbove' => 53, 'spentTime' => '77:2:0'],
                ['Severity' => 'Critical', 'Tracker' => 'Story', 'slaAbove' => 0, 'spentTime' => '16:28:17'],
                ['Severity' => 'Critical', 'Tracker' => 'Protocol Request', 'slaAbove' => 0, 'spentTime' => '13:8:17'],
            ],
            [
                'red' => 3,
                'yellow' => 1,
                'blue' => 1,
            ],
        ];
    }
}
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
