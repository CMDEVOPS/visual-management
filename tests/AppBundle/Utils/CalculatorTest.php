<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: spk
 * Date: 17/01/2019
 * Time: 13:24
 */

namespace Tests\AppBundle\Utils;

use AppBundle\Utils\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    protected $calculator;

    protected function setUp()
    {
        $this->calculator = new Calculator();
    }

    /**
     * @dataProvider getRemainingTimes
     */
    public function testCalculateRemainingTime($spendingTime, $resolution, $expected)
    {
        $this->assertSame($expected, $this->calculator->calculateRemainingTime($spendingTime, $resolution));
    }

    /**
     * @dataProvider getResponseTimes
     */
    public function testCalculateSlaTime(array $row, $resolution, array $expectation)
    {
        $result = $this->calculator->calculateSlaTime($row, $resolution);

        foreach ($expectation as $key => $expected) {
            $this->assertSame($expected, $result[$key]);
        }
    }

    public function getRemainingTimes()
    {
        yield [10000, 150, '147:13:20'];
        yield [95000, 75, '48:36:40'];
        yield [50000, 100, '86:6:40'];
    }

    public function getResponseTimes()
    {
        yield [
            ['ResponseTimeStatus3' => 31775, 'TimeEntsogclosed' => 8478],
            5,
            ['slaAbove' => 1, 'slaToShow' => '1:28:17', 'spentTime' => '6:28:17'],
        ];
        yield [
            ['ResponseTimeStatus3' => 1318573, 'TimeEntsogclosed' => 120921],
            24,
            ['slaAbove' => 308, 'slaToShow' => '308:40:52', 'spentTime' => '332:40:52'],
        ];
        yield [
            ['ResponseTimeStatus3' => 899950, 'TimeEntsogclosed' => 58334],
            24,
            ['slaAbove' => 209, 'slaToShow' => '209:46:56', 'spentTime' => '233:46:56'],
        ];
        yield [
            ['ResponseTimeStatus3' => 1176373, 'TimeEntsogclosed' => 139891],
            75,
            ['slaAbove' => 212, 'slaToShow' => '212:54:42', 'spentTime' => '287:54:42'],
        ];
    }
}
=======
<?php
/**
 * Created by PhpStorm.
 * User: spk
 * Date: 17/01/2019
 * Time: 13:24
 */

namespace Tests\AppBundle\Utils;

use AppBundle\Utils\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    protected $calculator;

    protected function setUp()
    {
        $this->calculator = new Calculator();
    }

    /**
     * @dataProvider getRemainingTimes
     */
    public function testCalculateRemainingTime($spendingTime, $resolution, $expected)
    {
        $this->assertSame($expected, $this->calculator->calculateRemainingTime($spendingTime, $resolution));
    }

    /**
     * @dataProvider getResponseTimes
     */
    public function testCalculateSlaTime(array $row, $resolution, array $expectation)
    {
        $result = $this->calculator->calculateSlaTime($row, $resolution);

        foreach ($expectation as $key => $expected) {
            $this->assertSame($expected, $result[$key]);
        }
    }

    public function getRemainingTimes()
    {
        yield [10000, 150, '147:13:20'];
        yield [95000, 75, '48:36:40'];
        yield [50000, 100, '86:6:40'];
    }

    public function getResponseTimes()
    {
        yield [
            ['ResponseTimeStatus3' => 31775, 'TimeEntsogclosed' => 8478],
            5,
            ['slaAbove' => 1, 'slaToShow' => '1:28:17', 'spentTime' => '6:28:17'],
        ];
        yield [
            ['ResponseTimeStatus3' => 1318573, 'TimeEntsogclosed' => 120921],
            24,
            ['slaAbove' => 308, 'slaToShow' => '308:40:52', 'spentTime' => '332:40:52'],
        ];
        yield [
            ['ResponseTimeStatus3' => 899950, 'TimeEntsogclosed' => 58334],
            24,
            ['slaAbove' => 209, 'slaToShow' => '209:46:56', 'spentTime' => '233:46:56'],
        ];
        yield [
            ['ResponseTimeStatus3' => 1176373, 'TimeEntsogclosed' => 139891],
            75,
            ['slaAbove' => 212, 'slaToShow' => '212:54:42', 'spentTime' => '287:54:42'],
        ];
    }
}
>>>>>>> c009e6fafa342eb93c461c493659bf7e585a84a1
